package pl.qdami.hh.mvcmock;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.setup.StandaloneMockMvcBuilder;
import pl.qdami.hh.spring.advice.ApiExceptionHandler;
import pl.qdami.hh.spring.configuration.WebConfiguration;

public class HhMock {

    public static StandaloneMockMvcBuilder standaloneSetup(Object... controllers) {
        return MockMvcBuilders.standaloneSetup(controllers)
                .setControllerAdvice(new ApiExceptionHandler())
                .setMessageConverters(createJackson2HttpMessageConverter());
    }

    private static MappingJackson2HttpMessageConverter createJackson2HttpMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        ObjectMapper objectMapper = builder.build();
        WebConfiguration.configureJacksonMapper(objectMapper);
        converter.setObjectMapper(objectMapper);
        return converter;
    }

}
