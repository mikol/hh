package pl.qdami.hh.todos.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.qdami.hh.json.JsonSerializer;
import pl.qdami.hh.mvcmock.HhMock;
import pl.qdami.hh.spring.exception.EntityNotFoundException;
import pl.qdami.hh.todos.dto.NewTodoRequestDto;
import pl.qdami.hh.todos.dto.TodoResponseDto;
import pl.qdami.hh.todos.dto.UpdateTodoRequestDto;
import pl.qdami.hh.todos.service.TodoService;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class TodoControllerTest {

    private static final String BASE_URL = "/api/todos";

    @Mock
    private TodoService todoService;

    private MockMvc mvc;
    private JsonSerializer jsonSerializer = new JsonSerializer();

    @Before
    public void setUp() throws Exception {
        TodoController todoController = new TodoController(todoService);
        mvc = HhMock.standaloneSetup(todoController).build();
    }

    @Test
    public void shouldAllowToCreateTodo() throws Exception {
        NewTodoRequestDto newTodoDto = new NewTodoRequestDto("some title", "some description");

        mvc.perform(
                post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSerializer.toJsonString(newTodoDto)))
                .andExpect(status().isOk());

        ArgumentCaptor<NewTodoRequestDto> argumentCaptor = ArgumentCaptor.forClass(NewTodoRequestDto.class);
        verify(todoService).createTodo(argumentCaptor.capture());
        NewTodoRequestDto todoToBePersisted = argumentCaptor.getValue();
        assertThat(todoToBePersisted).isEqualToComparingFieldByField(newTodoDto);
    }

    @Test
    public void shouldNotAllowToCreateTodo_whenTitleIsEmpty() throws Exception {
        NewTodoRequestDto newTodoDto = new NewTodoRequestDto(null, "some description");

        mvc.perform(
                post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSerializer.toJsonString(newTodoDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldAllowToGetTodo() throws Exception {
        mvc.perform(get(BASE_URL + "/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(todoService).getTodo(1L);
    }

    @Test
    public void shouldReturn404_whenGettingNotExistingTodo() throws Exception {
        doThrow(EntityNotFoundException.class).when(todoService).getTodo(1L);

        mvc.perform(get(BASE_URL + "/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldAllowToUpdateTodo() throws Exception {
        UpdateTodoRequestDto updateTodoDto = new UpdateTodoRequestDto("new title", "new description");

        mvc.perform(put(BASE_URL + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonSerializer.toJsonString(updateTodoDto)))
                .andExpect(status().isOk());

        ArgumentCaptor<UpdateTodoRequestDto> argumentCaptor = ArgumentCaptor.forClass(UpdateTodoRequestDto.class);
        verify(todoService).updateTodo(eq(1L), argumentCaptor.capture());
        UpdateTodoRequestDto todoToBePersisted = argumentCaptor.getValue();
        assertThat(todoToBePersisted).isEqualToComparingFieldByField(updateTodoDto);
    }

    @Test
    public void shouldReturn404_whenUpdatingNotExistingTodo() throws Exception {
        UpdateTodoRequestDto updateTodoDto = new UpdateTodoRequestDto("new title", "new description");
        doThrow(EntityNotFoundException.class).when(todoService).updateTodo(eq(1L), anyObject());

        mvc.perform(put(BASE_URL + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonSerializer.toJsonString(updateTodoDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldNotAllowToUpdateTodo_whenTitleIsEmpty() throws Exception {
        UpdateTodoRequestDto updateTodoDto = new UpdateTodoRequestDto(null, "new description");

        mvc.perform(put(BASE_URL + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonSerializer.toJsonString(updateTodoDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldAllowToDeleteTodo() throws Exception {
        mvc.perform(delete(BASE_URL + "/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(todoService).deleteTodo(1L);
    }

    @Test
    public void shouldReturn404_whenDeletingNotExistingTodo() throws Exception {
        doThrow(EntityNotFoundException.class).when(todoService).deleteTodo(1L);

        mvc.perform(delete(BASE_URL + "/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnTodoDetails_whenGettingOne() throws Exception {
        when(todoService.getTodo(1L)).thenReturn(TodoResponseDto.builder()
                .withId(1L)
                .withTitle("some title")
                .withDescription(Optional.of("some description"))
                .withDateFinish(Optional.of(LocalDateTime.of(2016, Month.DECEMBER, 1, 12, 15, 0)))
                .withDateIns(LocalDateTime.of(2016, Month.NOVEMBER, 5, 1, 8, 0))
                .build());

        mvc.perform(get(BASE_URL + "/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("some title")))
                .andExpect(jsonPath("$.description", is("some description")))
                .andExpect(jsonPath("$.dateFinish", is("2016-12-01T12:15:00")))
                .andExpect(jsonPath("$.dateIns", is("2016-11-05T01:08:00")));
    }

}