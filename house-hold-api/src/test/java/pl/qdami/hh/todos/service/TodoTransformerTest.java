package pl.qdami.hh.todos.service;

import org.junit.Test;
import pl.qdami.hh.todos.dto.TodoResponseDto;
import pl.qdami.hh.todos.model.Todo;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TodoTransformerTest {

    private TodoTransformer todoTransformer = new TodoTransformer();

    @Test
    public void shouldTransformTodoToDto() {
        // given
        LocalDateTime dateFinish = LocalDateTime.of(2016, Month.OCTOBER, 25, 0, 52);
        LocalDateTime dateIns = LocalDateTime.of(2016, Month.OCTOBER, 25, 0, 52);
        Todo todo = mock(Todo.class);
        when(todo.getId()).thenReturn(12L);
        when(todo.getName()).thenReturn("name");
        when(todo.getDescription()).thenReturn("description");
        when(todo.getDateFinish()).thenReturn(Timestamp.valueOf(dateFinish));
        when(todo.getDateIns()).thenReturn(Timestamp.valueOf(dateIns));

        // when
        TodoResponseDto todoResponseDto = todoTransformer.toDto(todo);

        // then
        assertThat(todoResponseDto.getId()).isEqualTo(12L);
        assertThat(todoResponseDto.getTitle()).isEqualTo("name");
        assertThat(todoResponseDto.getDescription()).hasValue("description");
        assertThat(todoResponseDto.getDateFinish()).hasValue(dateFinish);
        assertThat(todoResponseDto.getDateIns()).isEqualTo(dateIns);
    }

}