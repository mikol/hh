package pl.qdami.hh.todos.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.qdami.hh.members.model.Member;
import pl.qdami.hh.members.service.MemberService;
import pl.qdami.hh.spring.exception.EntityNotFoundException;
import pl.qdami.hh.todos.dto.NewTodoRequestDto;
import pl.qdami.hh.todos.dto.TodoResponseDto;
import pl.qdami.hh.todos.dto.UpdateTodoRequestDto;
import pl.qdami.hh.todos.model.Todo;
import pl.qdami.hh.todos.repository.TodosRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TodoServiceTest {

    @Mock
    private TodosRepository todosRepository;

    @Mock
    private TodoTransformer todoTransformer;

    @Mock
    private MemberService memberService;

    private TodoService todoService;

    @Before
    public void setUp() throws Exception {
        todoService = new TodoService(todosRepository, todoTransformer, memberService);
    }

    @Test
    public void shouldPersistBasicData_whenCreatingNewTodo() {
        // given
        NewTodoRequestDto todo = new NewTodoRequestDto("some todo", "some description");

        // when
        todoService.createTodo(todo);

        // then
        ArgumentCaptor<Todo> argumentCaptor = ArgumentCaptor.forClass(Todo.class);
        verify(todosRepository).save(argumentCaptor.capture());
        Todo persistedTodo = argumentCaptor.getValue();
        assertThat(persistedTodo.getName()).isEqualTo("some todo");
        assertThat(persistedTodo.getDescription()).isEqualTo("some description");
    }

    @Test
    public void shouldTemporaryAssignTodoToArtificialMember_whenCreatingNewTodo() {
        // given
        NewTodoRequestDto todo = new NewTodoRequestDto("some todo", "some description");
        Member artificialMember = mock(Member.class);
        when(memberService.getArtificialMember()).thenReturn(artificialMember);

        // when
        todoService.createTodo(todo);

        // then
        ArgumentCaptor<Todo> argumentCaptor = ArgumentCaptor.forClass(Todo.class);
        verify(todosRepository).save(argumentCaptor.capture());
        Todo persistedValue = argumentCaptor.getValue();
        assertThat(persistedValue.getMember()).isEqualTo(artificialMember);
    }

    @Test
    public void shouldReturnTodoById_whenItExists() {
        // given
        Todo persistedTodo = mock(Todo.class);
        TodoResponseDto todoResponseDto = mock(TodoResponseDto.class);
        when(todosRepository.findOne(1L)).thenReturn(persistedTodo);
        when(todoTransformer.toDto(persistedTodo)).thenReturn(todoResponseDto);

        // when
        TodoResponseDto foundTodo = todoService.getTodo(1L);

        // then
        assertThat(foundTodo).isEqualTo(todoResponseDto);
    }

    @Test
    public void shouldThrowException_whenGettingNotExistingTodo() {
        // given
        when(todosRepository.findOne(any())).thenReturn(null);

        // then
        assertThatExceptionOfType(EntityNotFoundException.class).isThrownBy(() -> todoService.getTodo(1L));
    }

    @Test
    public void shouldUpdateTodo_whenItExists() {
        // given
        Todo persistedTodo = new Todo();
        persistedTodo.setName("some name");
        persistedTodo.setDescription("some description");
        when(todosRepository.findOne(1L)).thenReturn(persistedTodo);

        // when
        UpdateTodoRequestDto updateTodoRequestDto = new UpdateTodoRequestDto("new name", "new description");
        todoService.updateTodo(1L, updateTodoRequestDto);

        // then
        ArgumentCaptor<Todo> argumentCaptor = ArgumentCaptor.forClass(Todo.class);
        verify(todosRepository).save(argumentCaptor.capture());
        Todo updatedTodo = argumentCaptor.getValue();
        assertThat(updatedTodo.getName()).isEqualTo("new name");
        assertThat(updatedTodo.getDescription()).isEqualTo("new description");
    }

    @Test
    public void shouldThrowException_whenUpdatingNotExistingTodo() {
        // given
        UpdateTodoRequestDto updateTodoRequestDto = mock(UpdateTodoRequestDto.class);
        when(todosRepository.findOne(any())).thenReturn(null);

        // then
        assertThatExceptionOfType(EntityNotFoundException.class).isThrownBy(() -> todoService.updateTodo(1L, updateTodoRequestDto));
    }

    @Test
    public void shouldDeleteTodo_whenItExists() {
        // given
        Todo persistedTodo = new Todo();
        persistedTodo.setName("some name");
        persistedTodo.setDescription("some description");
        when(todosRepository.findOne(1L)).thenReturn(persistedTodo);

        // when
        todoService.deleteTodo(1L);

        // then
        ArgumentCaptor<Todo> argumentCaptor = ArgumentCaptor.forClass(Todo.class);
        verify(todosRepository).delete(argumentCaptor.capture());
        Todo deletedTodo = argumentCaptor.getValue();
        assertThat(deletedTodo).isEqualTo(persistedTodo);
    }

    @Test
    public void shouldThrowException_whenDeleteingNotExistingTodo() {
        // given
        when(todosRepository.findOne(any())).thenReturn(null);

        // then
        assertThatExceptionOfType(EntityNotFoundException.class).isThrownBy(() -> todoService.deleteTodo(1L));
    }

}