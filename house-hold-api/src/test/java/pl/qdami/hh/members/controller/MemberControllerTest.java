package pl.qdami.hh.members.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.qdami.hh.members.dto.MemberResponseDto;
import pl.qdami.hh.members.service.MemberService;
import pl.qdami.hh.mvcmock.HhMock;
import pl.qdami.hh.spring.exception.EntityNotFoundException;
import pl.qdami.hh.todos.dto.TodoResponseDto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class MemberControllerTest {

    private static final String BASE_URL = "/api/members";

    @Mock
    private MemberService memberService;

    private MockMvc mvc;

    @Before
    public void setUp() throws Exception {
        MemberController memberController = new MemberController(memberService);
        mvc = HhMock.standaloneSetup(memberController).build();
    }

    @Test
    public void shouldAllowToGetMember() throws Exception {
        mvc.perform(get(BASE_URL + "/7").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(memberService).getMember(7L);
    }

    @Test
    public void shouldReturn404_whenGettingNotExistingMember() throws Exception {
        when(memberService.getMember(7L)).thenThrow(EntityNotFoundException.class);

        mvc.perform(get(BASE_URL + "/7").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldAllowToGetMembersTodos() throws Exception {
        mvc.perform(get(BASE_URL + "/7/todos").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(memberService).getTodosForMember(7L);
    }

    @Test
    public void shouldReturnMemberDetails_whenGettingOne() throws Exception {
        when(memberService.getMember(4L)).thenReturn(MemberResponseDto.builder()
                .withId(4L)
                .withFirstName("first name")
                .withSecondName(Optional.of("second name"))
                .withLastName("last name")
                .withSecondLastName(Optional.of("second last name"))
                .withNick(Optional.of("nick"))
                .withDefaultPhoneNumber(Optional.of("01212345678"))
                .withDateOfBirth(Optional.of(LocalDate.of(1979, Month.OCTOBER, 10)))
                .withDateIns(LocalDateTime.of(2016, Month.NOVEMBER, 5, 1, 1, 13))
                .build());

        mvc.perform(get(BASE_URL + "/4").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(4)))
                .andExpect(jsonPath("$.firstName", is("first name")))
                .andExpect(jsonPath("$.secondName", is("second name")))
                .andExpect(jsonPath("$.lastName", is("last name")))
                .andExpect(jsonPath("$.secondLastName", is("second last name")))
                .andExpect(jsonPath("$.nick", is("nick")))
                .andExpect(jsonPath("$.defaultPhoneNumber", is("01212345678")))
                .andExpect(jsonPath("$.dateOfBirth", is("1979-10-10")))
                .andExpect(jsonPath("$.dateIns", is("2016-11-05T01:01:13")));
    }

    @Test
    public void shouldReturnTodosDetails_whenGettingMembersTodos() throws Exception {
        LocalDateTime dateIns = LocalDateTime.of(2016, Month.OCTOBER, 25, 0, 52);
        LocalDateTime dateFinish = LocalDateTime.of(2017, Month.JANUARY, 1, 12, 0);
        when(memberService.getTodosForMember(4L)).thenReturn(Collections.singleton(TodoResponseDto.builder()
                .withId(3L)
                .withTitle("some title")
                .withDescription(Optional.of("some description"))
                .withDateIns(dateIns)
                .withDateFinish(Optional.of(dateFinish))
                .build()));

        mvc.perform(get(BASE_URL + "/4/todos").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(3)))
                .andExpect(jsonPath("$[0].title", is("some title")))
                .andExpect(jsonPath("$[0].description", is("some description")))
                .andExpect(jsonPath("$[0].dateIns", is("2016-10-25T00:52:00")))
                .andExpect(jsonPath("$[0].dateFinish", is("2017-01-01T12:00:00")));
    }
}