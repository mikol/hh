package pl.qdami.hh.members.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.qdami.hh.members.dto.MemberResponseDto;
import pl.qdami.hh.members.model.Member;
import pl.qdami.hh.members.repository.MembersRepository;
import pl.qdami.hh.spring.exception.EntityNotFoundException;
import pl.qdami.hh.todos.dto.TodoResponseDto;
import pl.qdami.hh.todos.model.Todo;
import pl.qdami.hh.todos.repository.TodosRepository;
import pl.qdami.hh.todos.service.TodoTransformer;

import java.util.Collections;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MemberServiceTest {

    @Mock
    private MembersRepository membersRepository;

    @Mock
    private TodosRepository todosRepository;

    @Mock
    private MemberTransformer memberTransformer;

    @Mock
    private TodoTransformer todoTransformer;

    private MemberService memberService;

    @Before
    public void setUp() throws Exception {
        memberService = new MemberService(membersRepository, todosRepository, memberTransformer, todoTransformer);
    }

    @Test
    public void shouldReturnMemberById_whenItExists() {
        // given
        Member member = mock(Member.class);
        MemberResponseDto memberResponseDto = mock(MemberResponseDto.class);
        when(membersRepository.findOne(1L)).thenReturn(member);
        when(memberTransformer.toDto(member)).thenReturn(memberResponseDto);

        // when
        MemberResponseDto foundMember = memberService.getMember(1L);

        // then
        assertThat(foundMember).isEqualTo(memberResponseDto);
    }

    @Test
    public void shouldThrowException_whenGettingNotExistingMember() {
        // given
        when(membersRepository.findOne(any())).thenReturn(null);

        // then
        assertThatExceptionOfType(EntityNotFoundException.class).isThrownBy(() -> memberService.getMember(1L));
    }

    @Test
    public void shouldReturnMembersTodos_whenMemberExists() {
        // given
        Member member = mock(Member.class);
        Todo todo = mock(Todo.class);
        Set<Todo> todos = Collections.singleton(todo);
        TodoResponseDto todoResponseDto = mock(TodoResponseDto.class);
        when(membersRepository.findOne(1L)).thenReturn(member);
        when(todosRepository.findByMemberId(1L)).thenReturn(todos);
        when(todoTransformer.toDto(todo)).thenReturn(todoResponseDto);

        // when
        Set<TodoResponseDto> todosFound = memberService.getTodosForMember(1L);

        // then
        assertThat(todosFound).containsExactly(todoResponseDto);
    }

    @Test
    public void shouldReturnEmptySet_whenNoTodosDefinedForMember() {
        // given
        Member member = mock(Member.class);
        when(membersRepository.findOne(1L)).thenReturn(member);
        when(member.getTodos()).thenReturn(Collections.emptySet());

        // when
        Set<TodoResponseDto> todosFound = memberService.getTodosForMember(1L);

        // then
        assertThat(todosFound).isEmpty();
    }

    @Test
    public void shouldReturnEmptySet_whenGettingTodosForNotExistingMember() {
        // given
        when(membersRepository.findOne(any())).thenReturn(null);

        // then
        assertThat(memberService.getTodosForMember(1L)).isEmpty();
    }

}