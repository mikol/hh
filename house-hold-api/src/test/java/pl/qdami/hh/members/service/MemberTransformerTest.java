package pl.qdami.hh.members.service;

import org.junit.Test;
import pl.qdami.hh.members.dto.MemberResponseDto;
import pl.qdami.hh.members.model.Member;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MemberTransformerTest {

    private MemberTransformer memberTransformer = new MemberTransformer();

    @Test
    public void shouldTransformMemberToDto() {
        // given
        LocalDate dateOfBirth = LocalDate.of(1979, Month.OCTOBER, 10);
        LocalDateTime dateIns = LocalDateTime.of(2016, Month.OCTOBER, 25, 0, 52);
        Member member = mock(Member.class);
        when(member.getId()).thenReturn(13L);
        when(member.getFirstName()).thenReturn("first name");
        when(member.getSecondName()).thenReturn("second name");
        when(member.getLastName()).thenReturn("last name");
        when(member.getSecondLastName()).thenReturn("second last name");
        when(member.getNick()).thenReturn("nick");
        when(member.getDefaultPhoneNumber()).thenReturn("01212345678");
        when(member.getDateOfBirth()).thenReturn(Date.valueOf(dateOfBirth));
        when(member.getDateIns()).thenReturn(Timestamp.valueOf(dateIns));

        // when
        MemberResponseDto memberResponseDto = memberTransformer.toDto(member);

        // then
        assertThat(memberResponseDto.getId()).isEqualTo(13L);
        assertThat(memberResponseDto.getFirstName()).isEqualTo("first name");
        assertThat(memberResponseDto.getSecondName()).hasValue("second name");
        assertThat(memberResponseDto.getLastName()).isEqualTo("last name");
        assertThat(memberResponseDto.getSecondLastName()).hasValue("second last name");
        assertThat(memberResponseDto.getNick()).hasValue("nick");
        assertThat(memberResponseDto.getDefaultPhoneNumber()).hasValue("01212345678");
        assertThat(memberResponseDto.getDateOfBirth()).hasValue(dateOfBirth);
        assertThat(memberResponseDto.getDateIns()).isEqualTo(dateIns);
    }

}