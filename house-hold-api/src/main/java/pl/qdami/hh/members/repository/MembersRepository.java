package pl.qdami.hh.members.repository;

import org.springframework.data.repository.CrudRepository;
import pl.qdami.hh.members.model.Member;

public interface MembersRepository extends CrudRepository<Member, Long> {
}
