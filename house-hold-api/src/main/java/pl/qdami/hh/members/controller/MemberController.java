package pl.qdami.hh.members.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.qdami.hh.members.dto.MemberResponseDto;
import pl.qdami.hh.members.service.MemberService;
import pl.qdami.hh.todos.dto.TodoResponseDto;

import java.util.Set;

@RestController
@RequestMapping("/api/members")
public class MemberController {

    private final MemberService memberService;

    MemberController(MemberService memberService) {
        this.memberService = memberService;
    }

    @RequestMapping(value = "/{memberId}", method = RequestMethod.GET)
    MemberResponseDto getMember(@PathVariable Long memberId) {
        return memberService.getMember(memberId);
    }

    @RequestMapping(value = "/{memberId}/todos", method = RequestMethod.GET)
    Set<TodoResponseDto> getTodosForMember(@PathVariable Long memberId) {
        return memberService.getTodosForMember(memberId);
    }

}
