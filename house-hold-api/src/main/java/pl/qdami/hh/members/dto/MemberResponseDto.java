package pl.qdami.hh.members.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MemberResponseDto {
    private final long id;
    private final String firstName;
    private final Optional<String> secondName;
    private final String lastName;
    private final Optional<String> secondLastName;
    private final Optional<String> nick;
    private final Optional<String> defaultPhoneNumber;
    private final Optional<LocalDate> dateOfBirth;
    private final LocalDateTime dateIns;

    private MemberResponseDto(Builder builder) {
        this.id = builder.id;
        this.firstName = builder.firstName;
        this.secondName = builder.secondName;
        this.lastName = builder.lastName;
        this.secondLastName = builder.secondLastName;
        this.nick = builder.nick;
        this.defaultPhoneNumber = builder.defaultPhoneNumber;
        this.dateOfBirth = builder.dateOfBirth;
        this.dateIns = builder.dateIns;
    }

    public static Builder builder() {
        return new Builder();
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Optional<String> getSecondName() {
        return secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public Optional<String> getSecondLastName() {
        return secondLastName;
    }

    public Optional<String> getNick() {
        return nick;
    }

    public Optional<String> getDefaultPhoneNumber() {
        return defaultPhoneNumber;
    }

    public Optional<LocalDate> getDateOfBirth() {
        return dateOfBirth;
    }

    public LocalDateTime getDateIns() {
        return dateIns;
    }

    public static class Builder {
        private long id;
        private String firstName;
        private Optional<String> secondName;
        private String lastName;
        private Optional<String> secondLastName;
        private Optional<String> nick;
        private Optional<String> defaultPhoneNumber;
        private Optional<LocalDate> dateOfBirth;
        private LocalDateTime dateIns;

        public Builder withId(long id) {
            this.id = id;
            return this;
        }

        public Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder withSecondName(Optional<String> secondName) {
            this.secondName = secondName;
            return this;
        }

        public Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder withSecondLastName(Optional<String> secondLastName) {
            this.secondLastName = secondLastName;
            return this;
        }

        public Builder withNick(Optional<String> nick) {
            this.nick = nick;
            return this;
        }

        public Builder withDefaultPhoneNumber(Optional<String> defaultPhoneNumber) {
            this.defaultPhoneNumber = defaultPhoneNumber;
            return this;
        }

        public Builder withDateOfBirth(Optional<LocalDate> dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public Builder withDateIns(LocalDateTime dateIns) {
            this.dateIns = dateIns;
            return this;
        }

        public MemberResponseDto build() {
            return new MemberResponseDto(this);
        }
    }
}
