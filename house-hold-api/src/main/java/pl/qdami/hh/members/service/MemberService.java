package pl.qdami.hh.members.service;

import org.springframework.stereotype.Service;
import pl.qdami.hh.members.dto.MemberResponseDto;
import pl.qdami.hh.members.model.Member;
import pl.qdami.hh.members.repository.MembersRepository;
import pl.qdami.hh.spring.exception.EntityNotFoundException;
import pl.qdami.hh.todos.dto.TodoResponseDto;
import pl.qdami.hh.todos.repository.TodosRepository;
import pl.qdami.hh.todos.service.TodoTransformer;

import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Service
public class MemberService {
    private static final long ARTIFICIAL_MEMBER_ID = 1L;

    private final MembersRepository membersRepository;
    private final TodosRepository todosRepository;
    private final MemberTransformer memberTransformer;
    private final TodoTransformer todoTransformer;

    MemberService(MembersRepository membersRepository, TodosRepository todosRepository, MemberTransformer memberTransformer, TodoTransformer todoTransformer) {
        this.membersRepository = membersRepository;
        this.todosRepository = todosRepository;
        this.memberTransformer = memberTransformer;
        this.todoTransformer = todoTransformer;
    }

    public MemberResponseDto getMember(Long memberId) {
        return Optional.ofNullable(membersRepository.findOne(memberId))
                .map(memberTransformer::toDto)
                .orElseThrow(() -> new EntityNotFoundException());
    }

    public Set<TodoResponseDto> getTodosForMember(Long memberId) {
        return todosRepository.findByMemberId(memberId).stream()
                .map(todoTransformer::toDto)
                .collect(toSet());
    }

    public Member getArtificialMember() {
        return Optional.ofNullable(membersRepository.findOne(ARTIFICIAL_MEMBER_ID))
                .orElseThrow(() -> new EntityNotFoundException());
    }

}
