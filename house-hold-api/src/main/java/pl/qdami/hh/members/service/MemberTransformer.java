package pl.qdami.hh.members.service;

import org.springframework.stereotype.Component;
import pl.qdami.hh.members.dto.MemberResponseDto;
import pl.qdami.hh.members.model.Member;

import java.sql.Date;
import java.util.Optional;

@Component
public class MemberTransformer {

    public MemberResponseDto toDto(Member model) {
        return MemberResponseDto.builder()
                .withId(model.getId())
                .withFirstName(model.getFirstName())
                .withSecondName(Optional.ofNullable(model.getSecondName()))
                .withLastName(model.getLastName())
                .withSecondLastName(Optional.ofNullable(model.getSecondLastName()))
                .withNick(Optional.ofNullable(model.getNick()))
                .withDefaultPhoneNumber(Optional.ofNullable(model.getDefaultPhoneNumber()))
                .withDateOfBirth(Optional.ofNullable(model.getDateOfBirth()).map(Date::toLocalDate))
                .withDateIns(model.getDateIns().toLocalDateTime())
                .build();
    }
}