package pl.qdami.hh.todos.model;

import pl.qdami.hh.members.model.Member;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Entity
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    private Timestamp dateFinish;
    @Column(insertable = false, updatable = false)
    private Timestamp dateIns;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_member", nullable = false)
    private Member member;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Timestamp dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Timestamp getDateIns() {
        return dateIns;
    }

    public void setDateIns(Timestamp dateIns) {
        this.dateIns = dateIns;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
