package pl.qdami.hh.todos.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.qdami.hh.todos.dto.NewTodoRequestDto;
import pl.qdami.hh.todos.dto.TodoResponseDto;
import pl.qdami.hh.todos.dto.UpdateTodoRequestDto;
import pl.qdami.hh.todos.service.TodoService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/todos")
public class TodoController {

    private final TodoService todoService;

    TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @RequestMapping(method = RequestMethod.POST)
    void createTodo(@RequestBody @Valid NewTodoRequestDto todoDto) {
        todoService.createTodo(todoDto);
    }

    @RequestMapping(value = "/{todoId}", method = RequestMethod.GET)
    TodoResponseDto getTodo(@PathVariable Long todoId) {
        return todoService.getTodo(todoId);
    }

    @RequestMapping(value = "/{todoId}", method = RequestMethod.PUT)
    void updateTodo(@PathVariable("todoId") Long todoId, @RequestBody @Valid UpdateTodoRequestDto todoDto) {
        todoService.updateTodo(todoId, todoDto);
    }

    @RequestMapping(value = "/{todoId}", method = RequestMethod.DELETE)
    void deleteTodo(@PathVariable Long todoId) {
        todoService.deleteTodo(todoId);
    }

}
