package pl.qdami.hh.todos.service;

import org.springframework.stereotype.Service;
import pl.qdami.hh.members.service.MemberService;
import pl.qdami.hh.spring.exception.EntityNotFoundException;
import pl.qdami.hh.todos.dto.NewTodoRequestDto;
import pl.qdami.hh.todos.dto.TodoResponseDto;
import pl.qdami.hh.todos.dto.UpdateTodoRequestDto;
import pl.qdami.hh.todos.model.Todo;
import pl.qdami.hh.todos.repository.TodosRepository;

import java.util.Optional;

@Service
public class TodoService {

    private final TodosRepository repository;
    private final TodoTransformer transformer;
    private final MemberService memberService;

    TodoService(TodosRepository repository, TodoTransformer transformer, MemberService memberService) {
        this.repository = repository;
        this.transformer = transformer;
        this.memberService = memberService;
    }

    public void createTodo(NewTodoRequestDto todoDto) {
        Todo todo = new Todo();
        todo.setName(todoDto.getTitle());
        todo.setDescription(todoDto.getDescription());
        // TODO (djaglowski): this should set currently logged in member, artificial member is just temporary solution
        todo.setMember(memberService.getArtificialMember());

        repository.save(todo);
    }

    public TodoResponseDto getTodo(Long todoId) {
        return Optional.ofNullable(repository.findOne(todoId))
                .map(transformer::toDto)
                .orElseThrow(() -> new EntityNotFoundException());
    }

    public void updateTodo(Long todoId, UpdateTodoRequestDto todoDto) {
        Todo todo = Optional.ofNullable(repository.findOne(todoId))
                .orElseThrow(() -> new EntityNotFoundException());

        todo.setName(todoDto.getTitle());
        todo.setDescription(todoDto.getDescription());

        repository.save(todo);
    }

    public void deleteTodo(Long todoId) {
        Todo todo = Optional.ofNullable(repository.findOne(todoId))
                .orElseThrow(() -> new EntityNotFoundException());

        repository.delete(todo);
    }
}
