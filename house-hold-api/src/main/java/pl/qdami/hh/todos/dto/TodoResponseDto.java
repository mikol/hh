package pl.qdami.hh.todos.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;
import java.util.Optional;

@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TodoResponseDto {
    private final long id;
    private final String title;
    private final Optional<String> description;
    private final Optional<LocalDateTime> dateFinish;
    private final LocalDateTime dateIns;

    private TodoResponseDto(Builder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.description = builder.description;
        this.dateFinish = builder.dateFinish;
        this.dateIns = builder.dateIns;
    }

    public static Builder builder() {
        return new Builder();
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Optional<String> getDescription() {
        return description;
    }

    public Optional<LocalDateTime> getDateFinish() {
        return dateFinish;
    }

    public LocalDateTime getDateIns() {
        return dateIns;
    }

    public static class Builder {
        private long id;
        private String title;
        private Optional<String> description;
        private Optional<LocalDateTime> dateFinish;
        private LocalDateTime dateIns;

        public Builder withId(long id) {
            this.id = id;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withDescription(Optional<String> description) {
            this.description = description;
            return this;
        }

        public Builder withDateFinish(Optional<LocalDateTime> dateFinish) {
            this.dateFinish = dateFinish;
            return this;
        }

        public Builder withDateIns(LocalDateTime dateIns) {
            this.dateIns = dateIns;
            return this;
        }

        public TodoResponseDto build() {
            return new TodoResponseDto(this);
        }
    }
}
