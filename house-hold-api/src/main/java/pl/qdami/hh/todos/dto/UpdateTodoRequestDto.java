package pl.qdami.hh.todos.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateTodoRequestDto {

    @NotBlank
    private final String title;
    private final String description;

    @JsonCreator
    public UpdateTodoRequestDto(@JsonProperty("title") String title,
                                @JsonProperty("description") String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

}
