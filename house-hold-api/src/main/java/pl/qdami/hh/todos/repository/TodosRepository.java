package pl.qdami.hh.todos.repository;

import org.springframework.data.repository.CrudRepository;
import pl.qdami.hh.todos.model.Todo;

import java.util.Set;

public interface TodosRepository extends CrudRepository<Todo, Long> {

    Set<Todo> findByMemberId(Long memberId);

}
