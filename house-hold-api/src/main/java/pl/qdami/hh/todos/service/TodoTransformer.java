package pl.qdami.hh.todos.service;

import org.springframework.stereotype.Component;
import pl.qdami.hh.todos.dto.TodoResponseDto;
import pl.qdami.hh.todos.model.Todo;

import java.sql.Timestamp;
import java.util.Optional;

@Component
public class TodoTransformer {

    public TodoResponseDto toDto(Todo todo) {
        return TodoResponseDto.builder()
                .withId(todo.getId())
                .withTitle(todo.getName())
                .withDescription(Optional.ofNullable(todo.getDescription()))
                .withDateFinish(Optional.ofNullable(todo.getDateFinish()).map(Timestamp::toLocalDateTime))
                .withDateIns(todo.getDateIns().toLocalDateTime())
                .build();
    }

}
