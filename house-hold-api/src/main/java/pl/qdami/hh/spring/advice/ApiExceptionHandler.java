package pl.qdami.hh.spring.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.qdami.hh.spring.exception.EntityNotFoundException;

@ControllerAdvice(basePackages = {"pl.qdami.hh"})
public class ApiExceptionHandler {

    @ExceptionHandler(value = EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    void handleEntityNotFoundException() {
    }

}
