CREATE TABLE member (
	id bigserial PRIMARY KEY,
	first_name character varying(20) NOT NULL,
	second_name character varying(20),
	last_name character varying(20) NOT NULL,
	second_last_name character varying(20),
	nick character varying(15),
	default_phone_number character varying (15),
	date_of_birth date,
    date_ins timestamp default localtimestamp(0)
);

CREATE TABLE todo (
	id bigserial PRIMARY KEY,
	id_member bigint REFERENCES member(id) NOT NULL,
	name character varying (100) NOT NULL,
	description text,
	date_finish timestamp,
    date_ins timestamp default localtimestamp(0)
);

INSERT INTO member(first_name, last_name) values('artificial', 'member');
