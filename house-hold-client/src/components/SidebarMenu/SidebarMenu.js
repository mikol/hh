import React from 'react';
import { Link } from 'react-router';
import { List, ListItem } from 'material-ui/List';
import Drawer from 'material-ui/Drawer';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import ActionEvent from 'material-ui/svg-icons/action/event';
import { lightBlack } from 'material-ui/styles/colors';
import * as classes from './SidebarMenu.scss';

export const SidebarMenu = () => (
  <Drawer>
    <h2 className={ classes['application-identity'] } style={{ color: lightBlack }}>House Hold</h2>
    <List>
      <Link to="/tasks"><ListItem primaryText="Zadania" leftIcon={ <ActionAssignment /> } /></Link>
      <Link to="/calendar"><ListItem primaryText="Kalendarz" leftIcon={ <ActionEvent /> } /></Link>
    </List>
    <Divider />
    <List>
      <Subheader>Statystyki</Subheader>
      <Link to="/stats/tasks"><ListItem primaryText="Realizacja zadań" /></Link>
      <Link to="/stats/other"><ListItem primaryText="Inne" /></Link>
    </List>
  </Drawer>
)

export default SidebarMenu;
