import React, { PropTypes, Component } from 'react';
import { Card, CardHeader, CardText } from 'material-ui/Card';
import Checkbox from 'material-ui/Checkbox';

export default class TaskList extends Component {

  render() {
    const { title, steps } = this.props;
    return (
      <Card>
        <CardHeader
          title={ title }
          actAsExpander={ true }
          showExpandableButton={ true }
        />
        {steps ? (
          <CardText expandable={ true }>
            { steps.map(step =>
                <Checkbox
                  key={ step.id }
                  checked={ step.done }
                  label={ step.description }
                  onClick={ console.log(step.id) }
                />
            )}
          </CardText>
        ) : null
      }
      </Card>
    )
  }
}

TaskList.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  steps: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    done: PropTypes.bool.isRequired,
    description: PropTypes.string.isRequired,
  })).isRequired
}
