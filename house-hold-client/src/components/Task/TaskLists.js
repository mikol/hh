import React, { PropTypes, Component } from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TaskList from './TaskList';
import NewTaskList from './NewTaskList';
import classes from './TaskLists.scss';

var taskId = 0;

export default class TaskLists extends Component {

  constructor(props) {
    super(props);
    this.state = {
      newTaskListOpened: false
    }
  }

  openNewTaskList = () => {
    this.setState({newTaskListOpened: true})
  }

  closeNewTaskList = () => {
    this.setState({newTaskListOpened: false})
  }

  saveNewTask = (task) => {
    const { addNewTask } = this.props;
    addNewTask({
      ...task,
      steps: [],
      id: taskId++
    });
    this.closeNewTaskList();
  }

  render() {
    const { tasks } = this.props;

    return (
      <div>
        <div className={ classes['task-list'] }>
          {tasks.length > 0 ?
            (
              tasks.map(task =>
                <TaskList
                  key={task.id}
                  {...task}
                />)
            ) :
            (
              <div className={ classes['task-list-empty-state'] }>
                <h3>Listy zadań.</h3>
                <p>Aby dodać nowe zadanie, kliknij przycisk w prawym dolnym rogu strony.<br/>
                <i>Wymyślić coś lepszego dla empty states na tej i pozostałych stronach.</i></p>
              </div>
            )
          }
        </div>
        <NewTaskList
          open={ this.state.newTaskListOpened }
          close={ this.closeNewTaskList }
          save={ this.saveNewTask }
        />
        <FloatingActionButton className={ classes['default-action-button'] } onTouchTap={ this.openNewTaskList }>
          <ContentAdd/>
        </FloatingActionButton>
      </div>
    )
  }
}

TaskLists.propTypes = {
  tasks: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
  })).isRequired
}

export default TaskLists;
