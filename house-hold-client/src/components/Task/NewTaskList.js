import React, { PropTypes, Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import { List, ListItem } from 'material-ui/List';
import Checkbox from 'material-ui/Checkbox';
import ContentAdd from 'material-ui/svg-icons/content/add';
import classes from './NewTaskList.scss'

const initialState = {
  title: null
}

export default class NewTaskList extends Component {

  constructor(props) {
    super(props);
    this.state = initialState;
  }

  componentWillUpdate = (nextProps, nextState) => {
    if(nextProps.open && !this.props.open) {
      this.resetState();
    }
  }

  resetState = () => {
    this.setState(initialState);
  }

  saveNewTask = () => {
    const { save } = this.props;
    const { title } = this.state;
    save({
      title
    });
  }

  handleChange = (field) => (event) => {
    this.setState({[field]: event.target.value});
  }

  render() {
    const { open, close } = this.props;
    const actions = [
      <FlatButton
        label="Anuluj"
        primary={ false }
        onTouchTap={ close }
      />,
      <FlatButton
        label="Zapisz"
        primary={ true }
        onTouchTap={ this.saveNewTask }
      />,
    ];

    return (
      <Dialog title="Lista zadań" modal={ false } open={ open } actions={ actions }>
        <TextField
          className={ classes['task-list-name'] }
          id={ "title" }
          name={ "title" }
          floatingLabelText="Nazwa"
          fullWidth={ true }
          onChange={ this.handleChange("title") }
        />
        <List>
          <ListItem
            style={{ paddingTop: 0 }}
            leftCheckbox={ <Checkbox /> }
            primaryText={
              <TextField
                  hintText="Zadanie"
                  fullWidth={ true }
                  multiLine={ true }
              />
            }
          />
          <ListItem
            style={{ paddingTop: 0 }}
            leftCheckbox={ <Checkbox /> }
            primaryText={
              <TextField
                  hintText="Zadanie"
                  fullWidth={ true }
                  multiLine={ true }
              />
            }
          />
          <ListItem
            style={{ paddingTop: 0 }}
            leftCheckbox={
              <Checkbox
                uncheckedIcon={ <ContentAdd /> }
                disabled={ true }
              />
            }
            primaryText={
              <TextField
                  hintText="Zadanie"
                  fullWidth={ true }
                  multiLine={ true }
              />
            }
          />
        </List>
      </Dialog>
    )
  }
}

NewTaskList.propTypes = {
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  save: PropTypes.func.isRequired
}
