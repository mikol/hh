import UserTaskLists from './containers/UserTaskLists'

export default [
  {
    path: 'tasks',
    component: UserTaskLists
  }
]
