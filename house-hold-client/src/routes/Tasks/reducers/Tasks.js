import * as ActionTypes from '../constants/ActionTypes';

const tasks = (state = [], action) => {
  switch (action.type) {
    case ActionTypes.ADD_TASK:
      return [
        {
          ...action.task,
          completed: false
        },
        ...state
      ];
    default:
      return state;
  }
}

export default tasks;
