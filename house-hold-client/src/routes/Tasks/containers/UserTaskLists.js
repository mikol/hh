import TaskLists from 'components/Task/TaskLists'
import { addTask } from '../actions'
import { connect } from 'react-redux'

const mapStateToProps = (state) => ({
    tasks: state.tasks,
})

export default connect(
  mapStateToProps,
  { addNewTask: addTask }
)(TaskLists)
