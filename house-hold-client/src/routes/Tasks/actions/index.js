import * as ActionTypes from '../constants/ActionTypes'

export const addTask = (task) => {
  return {
    type: ActionTypes.ADD_TASK,
    task: task
  }
};

export const deleteTask = (id) => {
  return {
    type: ActionTypes.DELETE_TASK,
    id
  }
};

export const editTask = (id) => {
  return {
    type: ActionTypes.EDIT_TASK,
    id
  }
};
