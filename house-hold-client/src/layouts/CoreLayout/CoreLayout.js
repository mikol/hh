import React from 'react';
import Header from '../../components/Header';
import SidebarMenu from '../../components/SidebarMenu';
import classes from './CoreLayout.scss';
import '../../styles/core.scss';

export const CoreLayout = ({ children }) => (
  <div>
    <SidebarMenu />
    <div className={classes.mainContainer}>
      <Header />
      {children}
    </div>
  </div>
)

CoreLayout.propTypes = {
  children: React.PropTypes.element.isRequired
}

export default CoreLayout
