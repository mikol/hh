# React client for House Hold application

## Table of Contents
1. [Requirements](#requirements)
1. [Used libraries](#used-libraries)
1. [Getting Started](#getting-started)
  1. [Running options](#running-options)

## Requirements
* node `^4.2.0`
* npm `^3.0.0`

You can download latest version of `Node.js` from [here](https://nodejs.org/en/).
After installing `Node.js` open command line and verify if its version and version of `npm` meets above requirements by calling:

```bash
node -v & npm -v
```

If there is a need to upgrade npm to the latest version, open command line (__with Administrator privileges__) and run:

```bash
npm install npm -g
```

## Used libraries
* [react](https://github.com/facebook/react)
* [redux](https://github.com/rackt/redux)
* [react-router](https://github.com/rackt/react-router)
* [react-router-redux](https://github.com/rackt/react-router-redux)
* [webpack](https://github.com/webpack/webpack)
* [babel](https://github.com/babel/babel)
* [koa](https://github.com/koajs/koa)
* [karma](https://github.com/karma-runner/karma)
* [eslint](http://eslint.org)

## Getting Started

__Notice:__ every time running `npm` commands make sure command line is run with __Administrator privileges__.

After confirming that your development environment meets the specified [requirements](#requirements), run the client from command line by calling:

```bash
$ npm install                   # Install project dependencies
$ npm start                     # Compile and launch
```

Go to [http://localhost:3000/](http://localhost:3000/) and see how beautilfully client is running :)

## Running options

While developing, you will probably rely mostly on `npm start`; however, there are additional scripts at your disposal:

|`npm run <script>`|Description|
|------------------|-----------|
|`start`|Serves your app at `localhost:3000`. HMR will be enabled in development.|
|`compile`|Compiles the application to disk (`~/dist` by default).|
|`dev`|Same as `npm start`, but enables nodemon for the server as well.|
|`dev:no-debug`|Same as `npm run dev` but disables devtool instrumentation.|
|`test`|Runs unit tests with Karma and generates a coverage report.|
|`test:dev`|Runs Karma and watches for changes to re-run tests; does not generate coverage reports.|
|`deploy`|Runs linter, tests, and then, on success, compiles your application to disk.|
|`deploy:dev`|Same as `deploy` but overrides `NODE_ENV` to "development".|
|`deploy:prod`|Same as `deploy` but overrides `NODE_ENV` to "production".|
|`lint`|Lint all `.js` files.|
|`lint:fix`|Lint and fix all `.js` files. [Read more on this](http://eslint.org/docs/user-guide/command-line-interface.html#fix).|
