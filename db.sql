CREATE TABLE human (
	id bigserial PRIMARY KEY,
	first_name character varying(20) NOT NULL,
	first_name2 character varying(20),
	last_name character varying(20) NOT NULL,
	last_name2 character varying(20),
	nick character varying(15),
	default_phone_number character varying (15),
	date_of_birth date,
    date_ins timestamp default localtimestamp(0)
);

CREATE TABLE todo (
	id bigserial PRIMARY KEY,
	id_human bigint REFERENCES human(id) NOT NULL,
	name character varying (100) NOT NULL,
	description text NOT NULL,
	date_finish timestamp,
    date_ins timestamp default localtimestamp(0)
);
